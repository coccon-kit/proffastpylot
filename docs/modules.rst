Modules
==================

This page list the internal methods of all modules, for developing purposes.

PROFFASTpylot consists of four main modules:

* **Pylot** contains the high level functionality,
* **Filemover** creates a consistent file structure from the PROFFAST output,
* **Prepare** derives all processing options from the input and
* **Pressure** reads and interpolates the pressure data.


Pylot
------

.. automodule:: pylot
   :members:
   :undoc-members:
   :show-inheritance:


Filemover
----------

.. automodule:: filemover
   :members:
   :undoc-members:
   :show-inheritance:


Prepare
---------

.. automodule:: prepare
   :members:
   :undoc-members:
   :show-inheritance:



Pressure
---------

.. automodule:: pressure
   :members:
   :undoc-members:
   :show-inheritance: