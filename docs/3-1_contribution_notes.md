# Contribution Notes

If you have any idea for improvement, find a bug or if you are missing a feature, you are welcome to contribute.
PROFFASTpylot already improved a lot because of feedback we received.

Please check first if your idea was implemented already in the `dev` branch. If not, you can either

- contact us directly via email, 
- create an issue here at Gitlab or
- implement the solution and make a merge request pointing to the `dev` branch.

We are happy about every contribution.


