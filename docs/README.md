## Build the documentation

A webversion of the documentation is available [here](https://www.imk-asf.kit.edu/english/4261.php).

To create the html version of the documentation, install sphinx with
 ```
 python -m pip install sphinx sphinx_mdinclude
 ```
and execute
```
./make html
```
inside the `docs` folder.



## Table of Contents
This is a short overview about the content of this documentation.

1. Getting Started  
	The first chapter gives you an introduction about the application.
	1. **Installation**  
		Please follow this installation guide before running PROFFASTpylot.
	2. **Usage**  
		This chapter explains how to use PROFFASTpylot.
	3. **Pressure Input**  
		Explains how the pressure is handled.
	4. **Folder structure**  
		Here, the file organization is explained.

2. User Information
	In this Chapter additional Information is provided.
	1. **All Input Parameters**  
		Lists all input parameters for lookup.
	2. **Time Offsets**  
		PROFFASTpylot can handle different time offsets, the internally used times are explained here
	3. **ILS Parameters**  
		A short information about the ILS Parameters that are provided with PROFFASTpylot.
	4. **Troubleshooting**  
		If you have a problem, check if it is solved already.

3. Developer Information
	1. **Contribution Notes**
