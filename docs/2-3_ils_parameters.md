# ILS Parameters

The ILS parameters are distributed together with PROFFASTpylot in the file `prfpylot/ILSList.csv`.
The correct parameters corresponding to the measurement time and instrument are read in automatically during runtime.
We recommend to use these official ILS parameters. In special cases you can use individual ILS parameters in the input file.

The `ILSList.csv` file will be updated in regular intervals, make sure to keep your version of PROFFASTpylot up to date.
