# PROFFASTpylot

Running PROFFAST with Python.  
This is a reimplementation of the Python scripts to run PROFFAST written by Qiansi Tu and Darko Dubravica.

PROFFASTpylot version 1.3 is an interface for PROFFAST 2.4.
For more information about PROFFAST, see
https://www.imk-asf.kit.edu/english/3225.php.

Information about the usage and installation of this package can be found in our [Documentation](https://www.imk-asf.kit.edu/english/4261.php).  
Please follow the installation instructions, first.

If you have any comments or questions, contact us at
benedikt.herkommer@kit.edu and lena.feld@kit.edu.
You are welcome to contribute.
